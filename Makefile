export ROOT_DIR = $(PWD)

include config/kernel.mk
include config/toolchain.mk

all:
	$(MAKE) -C kernel

clean:
	$(MAKE) clean -C kernel

install:
	$(MAKE) install -C kernel

test:
	$(MAKE) clean
	$(MAKE) all
	$(MAKE) install

.PHONY: all clean install
