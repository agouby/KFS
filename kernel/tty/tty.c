#include <tty.h>
#include <io.h>

struct s_tty	tty[MAXTTY] = {0};
struct s_tty	*tty_current = NULL;

size_t tty_scrollup(struct s_tty *tty)
{
	size_t pos;

	tty->x = 0;
	tty->y--;
	pos = VGA_GETPOS(tty->x, tty->y);
	if (tty == tty_current) {
		memmove(vga_baseaddr, vga_baseaddr + VGA_WIDTH, (VGA_BUFSIZE - VGA_WIDTH) * sizeof(*vga_baseaddr));
		for (int i = 0; i < VGA_WIDTH; i++) {
			vga_baseaddr[pos + i] = vga_entry(' ', tty->color);
		}
	}
	memmove(tty->buffer, tty->buffer + VGA_WIDTH, sizeof(*tty->buffer) * (VGA_BUFSIZE - VGA_WIDTH));
	memset(tty->buffer + VGA_BUFSIZE - VGA_WIDTH, ' ', VGA_WIDTH);
	for (int i = VGA_BUFSIZE - VGA_WIDTH; i < VGA_BUFSIZE; i++) {
		tty->buffer[i] = vga_entry(' ', tty->color);
	}
	return pos;
}

void	tty_fillscreen(struct s_tty *tty, int8_t c, uint16_t color)
{
	vga_fillscreen(vga_entry(c, color));
	memset(tty->buffer, c, sizeof(tty->buffer));
}

void	tty_clearscreen(struct s_tty *tty)
{
	tty_fillscreen(tty, ' ', vga_color(VGA_COLOR_LIGHTGRAY, VGA_COLOR_BLACK));
}

void	tty_setup(void)
{
	for (int i = 0; i < MAXTTY; i++) {
		tty[i].x = 0;
		tty[i].y = 0;
		tty_setcolor(&tty[i], vga_color(VGA_COLOR_LIGHTGRAY, VGA_COLOR_BLACK));
		tty_clearscreen(&tty[i]);
	}
	tty_current = &tty[0];
	vga_disablecursor();
}
