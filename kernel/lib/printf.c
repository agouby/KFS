#include <printf.h>

/* Read header file for infos */

static void	print_format_c(__attribute__((unused)) const struct s_conv *conv, va_list *ap)
{
	char c;

	c = (int)va_arg(*ap, int);
	tty_write(tty_current, &c, 1);
}

static void	print_format_d(__attribute__((unused)) const struct s_conv *conv, va_list *ap)
{
	int n;
	char buf[11];
	char *tmp;

	n = (int)va_arg(*ap, int);
	tmp = itoabase_buf(n, buf + sizeof(buf) - 1, 10);
	tty_write(tty_current, tmp, strlen(tmp));
}

static void	print_format_u(__attribute__((unused)) const struct s_conv *conv, va_list *ap)
{
	unsigned int n;
	char buf[11];
	char *tmp;

	n = (unsigned int)va_arg(*ap, unsigned int);
	tmp = utoabase_buf(n, buf + sizeof(buf) - 1, 10);
	tty_write(tty_current, tmp, strlen(tmp));
}

static void	print_format_s(__attribute__((unused)) const struct s_conv *conv, va_list *ap)
{
	char *s;

	s = (char *)va_arg(*ap, char *);
	if (!s)
		s = "(null)";
	tty_write(tty_current, s, strlen(s));
}

static void	print_format_o(__attribute__((unused)) const struct s_conv *conv, va_list *ap)
{
	unsigned int n;
	char buf[11];
	char *tmp;

	n = (unsigned int)va_arg(*ap, unsigned int);
	tmp = utoabase_buf(n, buf + sizeof(buf) - 1, 8);
	tty_write(tty_current, tmp, strlen(tmp));
}

static void	print_format_x(__attribute__((unused)) const struct s_conv *conv, va_list *ap)
{
	unsigned int n;
	char buf[11];
	char *tmp;

	n = (unsigned int)va_arg(*ap, unsigned int);
	tmp = utoabase_buf(n, buf + sizeof(buf) - 1, 16);
	tty_write(tty_current, "0x", 2);
	tty_write(tty_current, tmp, strlen(tmp));
}

static void	print_format_X(__attribute__((unused)) const struct s_conv *conv, va_list *ap)
{
	unsigned int n;
	char buf[11];
	char *tmp;

	n = (unsigned int)va_arg(*ap, unsigned int);
	tmp = utoabase_buf(n, buf + sizeof(buf) - 1, 16);
	tty_write(tty_current, "0x", 2);
	strtoupper(tmp);
	tty_write(tty_current, tmp, strlen(tmp));
}

static void	print_format_e(__attribute__((unused)) const struct s_conv *conv, va_list *ap)
{
	unsigned int n;

	n = (unsigned int)va_arg(*ap, unsigned int);
	if (n < VGA_COLOR_MAX)
		tty_setforegroundcolor(tty_current, (uint8_t)n);
}

static void	print_format_E(__attribute__((unused)) const struct s_conv *conv, va_list *ap)
{
	unsigned int n;

	n = (unsigned int)va_arg(*ap, unsigned int);
	if (n < VGA_COLOR_MAX)
		tty_setbackgroundcolor(tty_current, (uint8_t)n);
}

static void	print_format_b(__attribute__((unused)) const struct s_conv *conv, va_list *ap)
{
	unsigned int n;
	char buf[11];
	char *tmp;

	n = (unsigned int)va_arg(*ap, unsigned int);
	tmp = utoabase_buf(n, buf + sizeof(buf) - 1, 2);
	tty_write(tty_current, "0b", 2);
	tty_write(tty_current, tmp, strlen(tmp));
}

static void	print_format_p(__attribute__((unused)) const struct s_conv *conv, va_list *ap)
{
	print_format_x(conv, ap);
}

static void	print_format_perc(__attribute__((unused)) const struct s_conv *conv, __attribute__((unused)) va_list *ap)
{
	tty_write(tty_current, "%", 1);
}

static struct s_conv conv_data[] = {
	{'c', print_format_c},
	{'d', print_format_d},
	{'u', print_format_u},
	{'s', print_format_s},
	{'o', print_format_o},
	{'x', print_format_x},
	{'X', print_format_X},
	{'e', print_format_e},
	{'E', print_format_E},
	{'b', print_format_b},
	{'p', print_format_p},
	{'%', print_format_perc}
};

static const char	*get_format_string(const char *fmt, va_list *ap)
{
	struct s_conv *conv = NULL;

	for (size_t i = 0; i < COUNT_OF(conv_data); i++) {
		if (conv_data[i].specifier == *fmt) {
			conv = &conv_data[i];
			break ;
		}
	}
	if (conv)
		conv->func((const struct s_conv *)conv, ap);
	return (fmt + 1);
}

static void	vprintf(const char *fmt, va_list ap)
{
	while (*fmt)
	{
		if (*fmt == '%')
			fmt = get_format_string(fmt + 1, &ap);
		else
			tty_write(tty_current, fmt++, 1);
	}
}

int	printf(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
	return 0;
}
