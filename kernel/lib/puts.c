#include <puts.h>

int	tty_write(struct s_tty *tty, const char *buf, size_t n)
{
	size_t pos;

	for (size_t i = 0; i < n; i++) {
		if (tty->x >= VGA_WIDTH || buf[i] == '\n')
		{
			tty->x = 0;
			tty->y++;
		}
		else if (buf[i] == '\r')
			tty->x = 0;
		if (tty->y >= VGA_HEIGHT)
			pos = tty_scrollup(tty);
		else
			pos = VGA_GETPOS(tty->x, tty->y);

		if (isprint(buf[i]) && tty == tty_current)
		{
			vga_baseaddr[pos] = vga_entry(buf[i], tty->color);
			tty->x++;
		}
		tty->buffer[pos] = vga_entry(buf[i], tty->color);
	}
	return n;
}

void	tty_putchar(const char c)
{
	tty_write(tty_current, &c, 1);
}

void	tty_putstr(const char *s)
{
	tty_write(tty_current, s, strlen(s));
}
