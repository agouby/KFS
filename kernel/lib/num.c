#include <num.h>

/* itoa fills number from the back. To avoid having to reverse the string, we give the end pointer of ther buffer */

char	*itoabase_buf(int n, char *bufend, int base)
{
	char basestr[] = "0123456789abcdef";
	int neg = (n < 0) ? 1 : 0;

	*bufend-- = 0;
	do
		*bufend-- = basestr[ABS((n % base))];
	while (n /= base);
	if (neg)
	{
		*bufend = '-';
		return bufend;
	}
	else
		return (bufend + 1);
}

char	*utoabase_buf(unsigned int n, char *bufend, int base)
{
	char basestr[] = "0123456789abcdef";

	*bufend-- = 0;
	do
		*bufend-- = basestr[(n % base)];
	while (n /= base);
	return (bufend + 1);
}
