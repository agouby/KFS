#include <string.h>

void	*memset(void *s, unsigned char c, size_t n)
{
	for (size_t i = 0; i < n; i++) {
		((unsigned char *)s)[i] = c;
	}
	return s;
}

void	*memcpy(void *dest, const void *src, size_t n)
{
	char *tmp;
	const char *s;

	tmp = dest;
	s = src;
	while (n--)
		*tmp++ = *s++;
	return dest;
}

size_t strlen(const char *s)
{
	size_t i;

	for (i = 0; *s++; i++)
		;
	return i;
}

void *memmove(void *dst, const void *src, size_t len)
{
        size_t i;

        i = 0;
        if (src < dst)
        {
                while (len--)
                        ((unsigned char *)dst)[len] = ((unsigned char *)src)[len];
        }
        else
        {
                while (i < len)
                {
                        ((unsigned char *)dst)[i] = ((unsigned char *)src)[i];
                        i++;
                }
        }
        return dst;
}

char	*strchr(const char *s, char c)
{
	while (*s && *s != c)
		s++;
	return (c == *s) ? (char *)s : NULL;
}
