#include <vga.h>
#include <io.h>

uint16_t *vga_baseaddr = (uint16_t *)VGA_BASEADDR;

void	vga_fillscreen(uint16_t entry)
{
	uint16_t *base = (uint16_t *)VGA_BASEADDR;

	for (int i = 0; i < VGA_BUFSIZE; i++) {
			base[i] = entry;
	}
}

void	vga_clearscreen(void)
{
	vga_fillscreen(vga_entry(' ', vga_color(VGA_COLOR_LIGHTGRAY, VGA_COLOR_DARKGRAY)));
}

void	vga_enablecursor(uint8_t start, uint8_t end)
{
	uint16_t crtc;

	crtc = vga_crtc();
	outb(0xA, CRTC_ADDR(crtc));
	outb(start, CRTC_DATA(crtc));

	outb(0xB, CRTC_ADDR(crtc));
	outb(end, CRTC_DATA(crtc));
}

void	vga_disablecursor(void)
{
	uint16_t crtc;

	crtc = vga_crtc();
	outb(0xA, CRTC_ADDR(crtc));
	outb(0x20, CRTC_DATA(crtc));
}

uint16_t vga_crtc(void)
{
	return (inb(0x3CC) & 0x1) ? 0x3D4: 0x3B4;
}
