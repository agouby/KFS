#include <interrupt.h>
#include <string.h>
#include <io.h>
#include <pic.h>

void (*isr_pic_table[0x10])(void) = {
	[PIC_SYSTIME] = isr_systimer,
	[PIC_KEYBOARD] = isr_keyboard,
	[2] = isr_reserved,
	[PIC_SERIALCOM2] = isr_serialCOM2,
	[PIC_SERIALCOM1] = isr_serialCOM1,
	[PIC_LPTERM2] = isr_lpterm2,
	[PIC_FLOPPY] = isr_floppy,
	[PIC_LPTERM1] = isr_lpterm1,
	[PIC_RTC] = isr_rtc,
	[9] = isr_reserved,
	[10] = isr_reserved,
	[11] = isr_reserved,
	[PIC_MOUSE] = isr_mouse,
	[PIC_MATH] = isr_math,
	[PIC_ATA1] = isr_ata1,
	[PIC_ATA2] = isr_ata2
};

static wait(void)
{
	for (volatile unsigned int i = 0; i < 0xFFFFFF; i++)
		;
}

void	isr_default(int exception, int code)
{
	printf("INTERRUPT ! exception : %d, code : %d\n", exception, code);
	asm volatile("hlt");
}

void	isr_pic_handler(int entry)
{
	entry -= ICW2_MASTER_OFFSET;

	isr_pic_table[entry]();

	if (entry >= 8)
		outb(PIC_EIO, PIC_SLAVE_CMD);
	outb(PIC_EIO, PIC_MASTER_CMD);
}

void isr_systimer(void)
{

}

void isr_keyboard(void)
{
	static const unsigned char value_table_qwerty_shift[] = {
		0,
		0, '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '\b',
		0, 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '\n',
		0, 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ';', '\'', '~',
		0, '|', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?', 0,
		'*', 0, ' ', 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0,
		'7', '8', '9', '-',
		'4', '5', '6', '+',
		'1', '2', '3',
		'0', '.', '6',
		0, 0, 0, 0, 0,
	};

	static const unsigned char value_table_qwerty[] = {
		0,
		0, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '\b',
		0, 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',
		0, 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ':', '\"', '`',
		0, '\\', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', 0,
		'*', 0, ' ', 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0,
		'7', '8', '9', '-',
		'4', '5', '6', '+',
		'1', '2', '3',
		'0', '.', '6',
		0, 0, 0, 0, 0
	};

	uint8_t scancode;
	__attribute__((unused)) uint8_t pressed;

	scancode = inb(0x60);
	pressed = scancode & 0x80;
	scancode &= 0x7F;
	if (pressed)
		tty_write(tty_current, &value_table_qwerty[scancode], 1);
}

void isr_serialCOM2(void)
{

}

void isr_serialCOM1(void)
{

}

void isr_lpterm2(void)
{

}

void isr_lpterm1(void)
{

}

void isr_floppy(void)
{

}

void isr_rtc(void)
{

}

void isr_mouse(void)
{

}

void isr_math(void)
{

}

void isr_ata1(void)
{

}

void isr_ata2(void)
{

}

void isr_reserved(void)
{

}
