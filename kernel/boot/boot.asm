; Multiboot header

%define FLAG_ALIGN		1 << 0
%define FLAG_MEMINFO		1 << 1

%define MULTIBOOT_MAGIC		0x1BADB002
%define MULTIBOOT_FLAGS		(FLAG_ALIGN | FLAG_MEMINFO)
%define MULTIBOOT_CHECKSUM	-(MULTIBOOT_MAGIC + MULTIBOOT_FLAGS)

%define KERNEL_PAGEDIR_BASE	(_KERNEL_BASE >> 22)
; There are 1024 page directories.
; Since the kernel is starting at 3G, the kernel base is at (1024 * (3/4))
; (768 page directory)

%define STACK_SIZE	0x100

global _start
global _multiboot_ptr

extern _KERNEL_BASE
extern _EarlyPageDirectory
extern _early_paging
extern main

section .multiboot

dd MULTIBOOT_MAGIC
dd MULTIBOOT_FLAGS
dd MULTIBOOT_CHECKSUM

section .text

_start:
	call _early_paging

	lea ecx, [_start_HighHalf]
	jmp ecx

_start_HighHalf:
	mov dword [_EarlyPageDirectory], 0x0
	invlpg [0]
	mov esp, _stack + STACK_SIZE

	or ebx, _KERNEL_BASE
	mov [_multiboot_ptr], ebx
	; EBX holds multiboot header memory location
	; Since this is a physical address, we must convert it to virtual

	call main

	cli
_end:
	hlt
	jmp _end

section .bss
align 16
_stack:
	resb STACK_SIZE
_multiboot_ptr:
	resd 1
