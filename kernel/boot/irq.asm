extern isr_default
extern isr_pic_handler

%macro ISR_ISEXC 2
global _isr_%1
_isr_%1:
	pushad
	cld
	sub esp, 4
	push %1
	push 1
	call %2
	add esp, 0x8
	popad
	iret
%endmacro

%macro ISR_NOEXC 2
global _isr_%1
_isr_%1:
	pushad
	cld
	sub esp, 4
	push %1
	push 0
	call %2
	add esp, 0xC
	popad
	iret
%endmacro

%macro ISR_PIC 2
global _isr_%1
_isr_%1:
	pushad
	cld
	push %1
	call %2
	add esp, 0x4
	popad
	iret
%endmacro

ISR_NOEXC 0,isr_default
ISR_NOEXC 1,isr_default
ISR_NOEXC 2,isr_default
ISR_NOEXC 3,isr_default
ISR_NOEXC 4,isr_default
ISR_NOEXC 5,isr_default
ISR_NOEXC 6,isr_default
ISR_NOEXC 7,isr_default
ISR_NOEXC 8,isr_default
ISR_NOEXC 9,isr_default
ISR_NOEXC 10,isr_default
ISR_NOEXC 11,isr_default
ISR_NOEXC 12,isr_default
ISR_NOEXC 13,isr_default
ISR_NOEXC 14,isr_default
ISR_NOEXC 15,isr_default
ISR_NOEXC 16,isr_default
ISR_NOEXC 17,isr_default
ISR_NOEXC 18,isr_default
ISR_NOEXC 19,isr_default
ISR_NOEXC 20,isr_default
ISR_NOEXC 21,isr_default
ISR_NOEXC 22,isr_default
ISR_NOEXC 23,isr_default
ISR_NOEXC 24,isr_default
ISR_NOEXC 25,isr_default
ISR_NOEXC 26,isr_default
ISR_NOEXC 27,isr_default
ISR_NOEXC 28,isr_default
ISR_NOEXC 29,isr_default
ISR_NOEXC 30,isr_default
ISR_NOEXC 31,isr_default

ISR_PIC 32,isr_pic_handler
ISR_PIC 33,isr_pic_handler
ISR_PIC 34,isr_pic_handler
ISR_PIC 35,isr_pic_handler
ISR_PIC 36,isr_pic_handler
ISR_PIC 37,isr_pic_handler
ISR_PIC 38,isr_pic_handler
ISR_PIC 39,isr_pic_handler
ISR_PIC 40,isr_pic_handler
ISR_PIC 41,isr_pic_handler
ISR_PIC 42,isr_pic_handler
ISR_PIC 43,isr_pic_handler
ISR_PIC 44,isr_pic_handler
ISR_PIC 45,isr_pic_handler
ISR_PIC 46,isr_pic_handler
ISR_PIC 47,isr_pic_handler
ISR_PIC 48,isr_pic_handler

ISR_NOEXC 128,isr_default
