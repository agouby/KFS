#include <gdt.h>
#include <string.h>

static volatile struct s_gdt_ptr gdt_ptr;

void	gdt_set_entry(struct s_gdt_entry *entry, uint32_t base, uint32_t limit,
		      uint8_t access)
{
	entry->base_low = base & 0xFFFF;
	entry->base_mid = (base >> 16) & 0xFF;
	entry->base_high = (base >> 24) & 0xFF;

	entry->limit = limit & 0xFFFF;
	entry->granularity = 0xC0 | ((limit >> 16) & 0xF);
	entry->access = access;
}

/*
   Using flat memory model here, so base at 0 and limit at 4G.
   Segmentation is mandatory with intel, but we don't want to use it.
   So we must create a GDT, but we disable its effect by doing flat model.
   We want to use paging.
*/

void	gdt_set(void)
{
	struct s_gdt_entry *gdt = (struct s_gdt_entry *)GDT_BASEADDR;

	/* first gdt entry should always be null */
	memset(&gdt[0], 0, sizeof(gdt[0]));

	/* gdt entries of code, data and stack segments for kernel space */
	gdt_set_entry(&gdt[1], 0, 0xFFFFFFFF, 0x9B);
	gdt_set_entry(&gdt[2], 0, 0xFFFFFFFF, 0x93);
	gdt_set_entry(&gdt[3], 0, 0xFFFFFFFF, 0x97);

	/* gdt entries of code, data and stack segments for user space */
	gdt_set_entry(&gdt[4], 0, 0xFFFFFFFF, 0xFB);
	gdt_set_entry(&gdt[5], 0, 0xFFFFFFFF, 0xF3);
	gdt_set_entry(&gdt[6], 0, 0xFFFFFFFF, 0xF7);

	gdt_ptr.base = (uint32_t)gdt;
	gdt_ptr.limit = sizeof(gdt[0]) * 7 - 1;

	asm volatile("lgdt (gdt_ptr);"
		     "movw $0x10,%ax;"
		     "movw %ax,%ds;"
		     "movw %ax,%es;"
		     "movw %ax,%fs;"
		     "movw %ax,%gs;"
		     "movw %ax,%ss;"
		     "ljmp $0x8, $_next;"
		     "_next:;");
}
