#include <idt.h>
#include <string.h>
#include <gdt.h>
#include <interrupt.h>
#include <compiler.h>

static volatile struct s_idt_ptr idt_ptr;

static void	idt_set_entry(struct s_idt_entry *entry, uint32_t offset,
			      uint16_t selector, uint8_t gate, uint8_t dpl,
			      uint8_t used)
{
	entry->zero = 0;

	entry->offset_low = offset & 0xFFFF;
	entry->offset_high = (offset >> 0x10);

	entry->selector = selector;
	entry->type_attr = (gate & 0xF) | (dpl << 5) | (used << 7);
}

static void	idt_set_kinterrupt(struct s_idt_entry *entry,
				   void (*isr)(void), uint8_t used)
{
	idt_set_entry(entry, (size_t)isr, 0x8, INT_GATE, DPL_KERNEL, used);
} __attribute__((unused))

static void	idt_set_uinterrupt(struct s_idt_entry *entry,
				   void (*isr)(void), uint8_t used)
{
	idt_set_entry(entry, (size_t)isr, 0x8, INT_GATE, DPL_USER, used);
} __attribute__((unused))

void	idt_set(void)
{
	struct s_idt_entry *idt = (struct s_idt_entry *)IDT_BASEADDR;

	asm volatile("sti");

	idt_ptr.limit = sizeof(struct s_idt_entry) * IDT_NENTRIES - 1;
	idt_ptr.base = (uint32_t)idt;

	idt_set_kinterrupt(&idt[0], _isr_0, 1);
	idt_set_kinterrupt(&idt[1], _isr_1, 1);
	idt_set_kinterrupt(&idt[2], _isr_2, 1);
	idt_set_kinterrupt(&idt[3], _isr_3, 1);
	idt_set_kinterrupt(&idt[4], _isr_4, 1);
	idt_set_kinterrupt(&idt[5], _isr_5, 1);
	idt_set_kinterrupt(&idt[6], _isr_6, 1);
	idt_set_kinterrupt(&idt[7], _isr_7, 1);
	idt_set_kinterrupt(&idt[8], _isr_8, 1);
	idt_set_kinterrupt(&idt[9], _isr_9, 1);
	idt_set_kinterrupt(&idt[10], _isr_10, 1);
	idt_set_kinterrupt(&idt[11], _isr_11, 1);
	idt_set_kinterrupt(&idt[12], _isr_12, 1);
	idt_set_kinterrupt(&idt[13], _isr_13, 1);
	idt_set_kinterrupt(&idt[14], _isr_14, 1);
	idt_set_kinterrupt(&idt[15], _isr_15, 1);
	idt_set_kinterrupt(&idt[16], _isr_16, 1);
	idt_set_kinterrupt(&idt[17], _isr_17, 1);
	idt_set_kinterrupt(&idt[18], _isr_18, 1);
	idt_set_kinterrupt(&idt[19], _isr_19, 1);
	idt_set_kinterrupt(&idt[20], _isr_20, 1);
	idt_set_kinterrupt(&idt[21], _isr_21, 1);
	idt_set_kinterrupt(&idt[22], _isr_22, 1);
	idt_set_kinterrupt(&idt[23], _isr_23, 1);
	idt_set_kinterrupt(&idt[24], _isr_24, 1);
	idt_set_kinterrupt(&idt[25], _isr_25, 1);
	idt_set_kinterrupt(&idt[26], _isr_26, 1);
	idt_set_kinterrupt(&idt[27], _isr_27, 1);
	idt_set_kinterrupt(&idt[28], _isr_28, 1);
	idt_set_kinterrupt(&idt[29], _isr_29, 1);
	idt_set_kinterrupt(&idt[30], _isr_30, 1);
	idt_set_kinterrupt(&idt[31], _isr_31, 1);
	idt_set_kinterrupt(&idt[32], _isr_32, 1);
	idt_set_kinterrupt(&idt[33], _isr_33, 1);
	idt_set_kinterrupt(&idt[34], _isr_34, 1);
	idt_set_kinterrupt(&idt[35], _isr_35, 1);
	idt_set_kinterrupt(&idt[36], _isr_36, 1);
	idt_set_kinterrupt(&idt[37], _isr_37, 1);
	idt_set_kinterrupt(&idt[38], _isr_38, 1);
	idt_set_kinterrupt(&idt[39], _isr_39, 1);
	idt_set_kinterrupt(&idt[40], _isr_40, 1);
	idt_set_kinterrupt(&idt[41], _isr_41, 1);
	idt_set_kinterrupt(&idt[42], _isr_42, 1);
	idt_set_kinterrupt(&idt[43], _isr_43, 1);
	idt_set_kinterrupt(&idt[44], _isr_44, 1);
	idt_set_kinterrupt(&idt[45], _isr_45, 1);
	idt_set_kinterrupt(&idt[46], _isr_46, 1);
	idt_set_kinterrupt(&idt[47], _isr_47, 1);
	idt_set_kinterrupt(&idt[48], _isr_48, 1);
	idt_set_kinterrupt(&idt[128], _isr_128, 1);

	asm volatile("lidt (idt_ptr)");
}
