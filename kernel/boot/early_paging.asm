%define KERNEL_PAGEDIR_BASE (_KERNEL_BASE >> 22)

extern _KERNEL_BASE
extern _kernel_start
extern _kernel_end

section .data

_EarlyPageDirectory times 1024 dd 0x0

section .text

global _early_paging
global _EarlyPageDirectory

_fill_pagetable:
	mov DWORD[edi], esi
	or DWORD[edi], 3
	add edi, 4
	add esi, 4096
	sub ecx, 4096
	jnz _fill_pagetable
	ret

_early_paging:
	mov eax, _KERNEL_BASE
	shr eax, 20
	; eax contains the address of the kernel entry in the page directory
	; (768 * 4)

	mov edi, _EarlyPageDirectory
	sub edi, _KERNEL_BASE
	add edi, eax
	mov DWORD[edi], 0x700003
	mov edi, 0x700000
	mov esi, 0
	mov ecx, _kernel_end
	sub ecx, _KERNEL_BASE
	call _fill_pagetable
	; Fill page table for kernel mapping at _KERNEL_BASE

	mov edi, _EarlyPageDirectory
	sub edi, _KERNEL_BASE
	mov DWORD[edi], 0x400003
	mov edi, 0x400000
	mov esi, 0
	mov ecx, _kernel_end
	sub ecx, _KERNEL_BASE
	call _fill_pagetable
	; Fill page table for kernel mapping at 0x0

	mov edi, _EarlyPageDirectory
	sub edi, _KERNEL_BASE
	sub eax, 4
	add edi, eax
	mov DWORD[edi], 0x6FF003
	mov edi, 0x6FF000
	mov esi, 0x400000
	mov ecx, 0x400000
	call _fill_pagetable
	; Fill page table for VMM mapping at 0xBFC00000

	mov ecx, _EarlyPageDirectory
	sub ecx, _KERNEL_BASE
	mov cr3, ecx
	; Placing page directory address in cr3

	mov ecx, cr0
	or ecx, 0x80010000
	mov cr0, ecx
	; Enabling paging and setting write protect to 1
	; This way writing to a read only page triggers an interrupt

	ret
