#include <pic.h>

void	pic_enable_mask(uint8_t line)
{
	uint8_t port;
	uint8_t mask;

	if (line >= 8)
	{
		port = PIC_SLAVE_DATA;
		line -= 8;
	}
	else
		port = PIC_MASTER_DATA;
	mask = inb(port);
       	mask &= ~(1 << line);
	outb(mask, port);
}

void	pic_disable_mask(uint8_t line)
{
	uint8_t port;
	uint8_t mask;

	if (line >= 8)
	{
		port = inb(PIC_SLAVE_DATA);
		line -= 8;
	}
	else
		port = inb(PIC_MASTER_DATA);
	mask = inb(port) | (1 << line);
	outb(mask, port);
}

void	pic_init(void)
{
	/* ICW1 */
	outb(ICW1_ENABLE | ICW1_ICW4_IS_PRESENT, PIC_MASTER_CMD);
	outb(ICW1_ENABLE | ICW1_ICW4_IS_PRESENT, PIC_SLAVE_CMD);

	/* ICW2 */
	outb(ICW2_MASTER_OFFSET, PIC_MASTER_DATA);
	outb(ICW2_SLAVE_OFFSET, PIC_SLAVE_DATA);

	/* ICW3 */
	outb(ICW3_MASTER_IRQ2, PIC_MASTER_DATA);
	outb(ICW3_SLAVE_IRQ2, PIC_SLAVE_DATA);

	/* ICW4 */
	outb(ICW4_80x86_MODE, PIC_MASTER_DATA);
	outb(ICW4_80x86_MODE, PIC_SLAVE_DATA);

	/* Clear all masks */
	outb(0xFF, PIC_MASTER_DATA);
	outb(0xFF, PIC_SLAVE_DATA);

	pic_enable_mask(PIC_KEYBOARD);
}
