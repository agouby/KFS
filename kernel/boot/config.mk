LINKER_SCRIPT	:= boot/linker.ld

ASM_SOURCES	+= boot/boot.asm	 \
		   boot/early_paging.asm \
		   boot/irq.asm

C_SOURCES	+= boot/gdt.c		\
		   boot/idt.c		\
		   boot/interrupt.c	\
		   boot/isr.c		\
		   boot/pic.c
