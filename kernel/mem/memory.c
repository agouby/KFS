#include <memory.h>

void	memory_init(uint32_t low, uint32_t high)
{
	frames_init(low, high);

	get_free_frame(0);
	paging_init();
}
