#include <memory.h>
#include <printf.h>

/*
 * We are using a bitmap technique to store frames.
 * A frame is 4KB of memory. We have to identify which ones are used,
 * and the ones that are free. So we can use a single bit to flag an entire map,
 * and a byte to flag 8 pages.
 */

uint8_t frame_bitmap[MAX_BITMAP];
static uint32_t	nframes;

/*
 * Here we need :
 * A function to :
 *	- Free a frame
 *	- Allocate a frame
 *	- Find a free frame
 */

/*
 * This function will search for a free frame and return it.
 * The address of the frame should be the index in frame_bitmap * the index in the byte * 4096
 */

static inline int get_bit_pos(uint8_t byte)
{
	int index = 0;

	for (int pos = 0x1; (byte & pos); pos <<= 1)
		++index;
	return index;
}

static uint32_t search_free_frames(uint32_t *start, uint32_t end, uint32_t n)
{
	uint8_t bit;
	uint32_t ret;

	while (*start <= end)
	{
		if (frame_bitmap[*start] != 0xFF)
		{
			bit = get_bit_pos(frame_bitmap[*start]);
			for (uint32_t n_tmp = 1; n_tmp < n; n_tmp++)
			{
				if (bit == 7)
				{
					if (*start == end)
						goto err;
					(*start)++;
					bit = 0;
				}
				else
					bit++;
				if (frame_bitmap[*start] & bit)
					break ;
			}
			ret = GET_BITMAP_ADDR(*start, bit) - PAGE_ADDR(n - 1);
			start += (n >> 0x3);
			return ret;
		}
		else
			(*start)++;
	}
err:
	return FRAME_ERR;
}

uint32_t get_free_frame(uint32_t n)
{
	static uint32_t start;
	uint32_t start_old;
	uint32_t bitmap_end;
	uint32_t ret;

	bitmap_end = nframes >> 0x3;
	start_old = start;
	ret = search_free_frames(&start, bitmap_end, n);
	if (ret != FRAME_ERR)
		return ret;
	start = ((uint32_t)KERNEL_END >> PAGE_SHIFT) >> 0x3;
	return search_free_frames(&start, start_old, n);
}

void	*allocate_frame(uint32_t frame)
{
	int bitmap_index;
	int bit_index;

	if (frame == FRAME_ANY)
	{
		frame = get_free_frame(1);
		bitmap_index = GET_FRAME_BITMAP(frame);
		bit_index = GET_BITMAP_BIT_INDEX(frame);
		SET_BIT(frame_bitmap[bitmap_index], bit_index);
		return (void *)GET_BITMAP_ADDR(bitmap_index, bit_index);
	}
	if (unlikely(frame & PAGE_ALIGN_MASK))
	{
		/*
		 *This should never happend. It means the page is not aligned on 4KB.
		 * Either we throw a warning and do nothing, or we do something stronger.
		 * For now we just return NULL.
		 */
		printf("Requested frame if not aligned on PAGE_SIZE\n");
		return NULL;
	}
	bitmap_index = GET_FRAME_BITMAP(frame);
	bit_index = GET_BITMAP_BIT_INDEX(frame);
	if (frame_bitmap[bitmap_index] & (1 << bit_index))
	{
		/* Frame is already allocated, return NULL*/
		printf("Frame %x is already allocated.\n", frame);
		return NULL;
	}
	SET_BIT(frame_bitmap[bitmap_index], bit_index);
	return (void *)frame;
}

/* Freeing a frame is easy since it is contigous in bitmap. */
void	free_frame(uint32_t frame)
{
	int bitmap_index;
	int bit_index;

	if (unlikely(frame & PAGE_ALIGN_MASK))
	{
		/*
		 * This should never happend. It means the page is not aligned on 4KB.
		 * Either we throw an warning and do nothing, or we do something stronger.
		 * For now we just return.
		 */
		printf("Requested frame is not aligned on 4KB, ignored.\n");
		return ;
	}
	bitmap_index = GET_FRAME_BITMAP(frame);
	bit_index = GET_BITMAP_BIT_INDEX(frame);

	CLEAR_BIT(frame_bitmap[bitmap_index], bit_index);
}

void	frames_init(uint32_t low, uint32_t high)
{
	/* multiboot mem_* values are in KB */
	nframes = ((low + high) << 8) >> PAGE_SHIFT;

	memset(frame_bitmap, 0, MAX_BITMAP);
}
