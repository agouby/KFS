#include <memory.h>
#include <printf.h>

/* CR3 register expects a page aligned address */
static uint32_t page_dir[1024] __align(PAGE_SIZE);


static uint32_t *get_pde(uint32_t page)
{
	return &page_dir[page >> 0x16];
}

static uint32_t *get_pte(uint32_t *pde, uint32_t page)
{
	uint32_t offset;
	uint32_t *addr;


	addr = (uint32_t *)(PT_ADDRESS(*pde) - VMM_BASEPADDR);
	offset = PTE_INDEX(page >> 0xC);
	return (VMM_BASEVADDR + (void *)addr + (offset << 2));
}

static uint32_t get_pt_physaddr(void *pde)
{
	uint32_t offset;

	offset = (pde - (void *)page_dir) >> 2;
	return (VMM_BASEPADDR + (offset << PAGE_SHIFT));
}

static void reload_page_dir(void)
{
	/* load page_dir in cr3 */
	uint32_t lol;

	lol = (void *)page_dir - KERNEL_BASE;
	asm volatile("movl %0, %%cr3" :: "a" (lol));
}

void	invlpg(void *page)
{
	 asm volatile("invlpg (%0)" : : "b"(page) : "memory");
}

static int _map_page(uint32_t frame, uint32_t page, uint32_t flags)
{
	uint32_t *pde;
	uint32_t *pte;

	pde = get_pde(page);

	if (*pde & PAGE_PRESENT)
	{
		/* A page table exists, use it. */
		pte = get_pte(pde, page);
	}
	else
	{
		*pde = 0;
		PAGE_SET(*pde, get_pt_physaddr(pde));
		PAGE_SET(*pde, PAGE_PRESENT);
		PAGE_SET(*pde, PAGE_WRITE);
		pte = get_pte(pde, page);
	}

	*pte = 0;
	PAGE_SET(*pte, frame);
	PAGE_SET(*pte, PAGE_PRESENT);
	PAGE_SET(*pte, flags);
	allocate_frame(frame);
	return 0;
}

int map_page(uint32_t phys, uint32_t virt, uint32_t flags)
{
	_map_page(phys, virt, flags);
	reload_page_dir();
	return 0;
}

int unmap_page(uint32_t page)
{
	uint32_t *pde;
	uint32_t *pte;
	uint32_t *pt_base;
	uint32_t present;

	present = 0;
	pde = get_pde(page);
	if (unlikely(*pde & PAGE_PRESENT) == 0)
		return -1;

	pt_base = (uint32_t *)(PT_ADDRESS(*pde) - VMM_BASEPADDR);
	pt_base = (void *)pt_base + VMM_BASEVADDR;
	pte = get_pte(pde, page);
	for (int i = 0; i < 1024; i++, pt_base += 1)
	{
		if (pt_base == pte)
		{
			free_frame(PT_ADDRESS(*pte));
			memset(pte, 0, sizeof(*pte));
			invlpg((void *)page);
		}
		if (!present && *pt_base & PAGE_PRESENT)
			present = 1;
	}
	if (!present)
		memset(pde, 0, sizeof(*pde));
	return 0;
}

int _map_page_range(uint32_t phys, uint32_t virt, uint32_t cnt, uint32_t flags)
{
	if ((phys != FRAME_ANY && unlikely(phys & PAGE_ALIGN_MASK))
	    || unlikely(virt & PAGE_ALIGN_MASK)
	    || unlikely(cnt & PAGE_ALIGN_MASK))
	{
		/* Should never happend */
		printf("map_page_range: Something is not aligned\n");
		return -1;
	}
	for (uint32_t i = 0; i < cnt; i += PAGE_SIZE)
	{
		if (phys == FRAME_ANY)
			_map_page(FRAME_FREE(1), virt, flags);
		else
		{
			_map_page(phys, virt, flags);
			phys += PAGE_SIZE;
		}
		virt += PAGE_SIZE;
	}
	return 0;
}

int map_page_range(uint32_t phys, uint32_t virt, uint32_t cnt, uint32_t flags)
{
	_map_page_range(phys, virt, cnt, flags);
	reload_page_dir();
	return 0;
}

void	paging_init(void)
{
	_map_page_range(0x0, (uint32_t)KERNEL_BASE, _1MB_, PAGE_WRITE);

	_map_page_range(KSECT_PHYS(text), KSECT(text, start), KSECT_SIZE(text), PAGE_WRITE);

	_map_page_range(KSECT_PHYS(rodata), KSECT(rodata, start), KSECT_SIZE(rodata), 0);

	_map_page_range(KSECT_PHYS(data), KSECT(data, start), KSECT_SIZE(data), PAGE_WRITE);

	_map_page_range(KSECT_PHYS(bss), KSECT(bss, start), KSECT_SIZE(bss), PAGE_WRITE);

	/* Reserve 4MB of memory for VMM from VMM_BASEVADDR */
	_map_page_range(VMM_BASEPADDR, VMM_BASEVADDR, _4MB_, PAGE_WRITE);

	reload_page_dir();
}
