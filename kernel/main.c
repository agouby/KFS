#include <tty.h>
#include <io.h>
#include <gdt.h>
#include <idt.h>
#include <pic.h>
#include <printf.h>
#include <multiboot.h>
#include <memory.h>

/*
 * We use _kernel_start and _kernel_end variables from linker file to determine
 * the size of the kernel. This will be helpfull to map the kernel pages.
 */

extern struct multiboot_info *_multiboot_ptr;

void	main(void)
{
	/* Initialization of the GDT table */
	gdt_set();

	/* Initialization of the IDT table */
	idt_set();

	/* Initialization of the PIC chip */
	pic_init();

	/* Initialization of all the tty */
	tty_setup();

	/* Initialization of the memory */
	memory_init(_multiboot_ptr->mem_lower, _multiboot_ptr->mem_upper);

	/* Initialization of the stack with relocation */
	stack_init();
	asm volatile("mov %[r], %%esp" :: [r] "r" (STACK_PTR));

	/* Initialization of the heap */
	heap_init();

	while (1)
		asm volatile("hlt");
}
