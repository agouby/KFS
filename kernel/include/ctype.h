#ifndef CTYPE_H
#define CTYPE_H

static inline char islower(char c)
{
	return (c >= 'a' && c <= 'z');
};

static inline char toupper(char c)
{
	if (islower(c))
		c -= 32;
	return c;
};

static inline char *strtoupper(char *s)
{
	char *tmp = s;

	while (*s) {
		*s = toupper(*s);
		s++;
	}
	return tmp;
};


static inline int isprint(unsigned char c)
{
	return (c >= 32 && c <= 126);
}

#endif
