#ifndef INTERRUPT_H
#define INTERRUPT_H

#include <types.h>

extern void (*isr_pic_table[0x10])(void);

void _isr_0(void);
void _isr_1(void);
void _isr_2(void);
void _isr_3(void);
void _isr_4(void);
void _isr_5(void);
void _isr_6(void);
void _isr_7(void);
void _isr_8(void);
void _isr_9(void);
void _isr_10(void);
void _isr_11(void);
void _isr_12(void);
void _isr_13(void);
void _isr_14(void);
void _isr_15(void);
void _isr_16(void);
void _isr_17(void);
void _isr_18(void);
void _isr_19(void);
void _isr_20(void);
void _isr_21(void);
void _isr_22(void);
void _isr_23(void);
void _isr_24(void);
void _isr_25(void);
void _isr_26(void);
void _isr_27(void);
void _isr_28(void);
void _isr_29(void);
void _isr_30(void);
void _isr_31(void);
void _isr_32(void);
void _isr_33(void);
void _isr_34(void);
void _isr_35(void);
void _isr_36(void);
void _isr_37(void);
void _isr_38(void);
void _isr_39(void);
void _isr_40(void);
void _isr_41(void);
void _isr_42(void);
void _isr_43(void);
void _isr_44(void);
void _isr_45(void);
void _isr_46(void);
void _isr_47(void);
void _isr_48(void);
void _isr_128(void);

void	isr_default(int exception, int code);
void	isr_pic_handler(int entry);

void	isr_systimer(void);
void	isr_keyboard(void);
void	isr_serialCOM2(void);
void	isr_serialCOM1(void);
void	isr_lpterm2(void);
void	isr_lpterm1(void);
void	isr_floppy(void);
void	isr_rtc(void);
void	isr_mouse(void);
void	isr_math(void);
void	isr_ata1(void);
void	isr_ata2(void);

void	isr_reserved(void);

#endif
