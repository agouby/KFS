#ifndef PUTS_H
#define PUTS_H

#include <vga.h>
#include <tty.h>
#include <ctype.h>

int	tty_write(struct s_tty *tty, const char *buf, size_t n);
void	tty_putchar(const char c);
void	tty_putstr(const char *s);

#endif
