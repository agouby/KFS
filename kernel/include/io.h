#ifndef IO_H
#define IO_H

#include <types.h>
#include <compiler.h>

static inline void outb(uint8_t val, uint16_t port)
{
	asm volatile("outb %0,%1" :: "a" (val), "d" (port));
}

static inline uint8_t inb(uint16_t port)
{
	uint8_t ret;

	asm volatile("inb %1,%0" : "=a" (ret) : "d" (port));
	return ret;
}

#endif
