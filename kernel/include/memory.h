#ifndef MEMORY_H
#define MEMORY_H

#include <types.h>
#include <string.h>
#include <multiboot.h>

/* PAGE_SIZE should always be 4KB and must not be changed.
   PAGE_SIZE == FRAME_SIZE
 * PAGE_SHIFT is here to save some divisions and multiplications.
 * (x * 4096) == (x << 0xC)
 * PAGE_ALIGN_MASK helps to check if an address is aligned on PAGE_SIZE.
 */
#define PAGE_SIZE	0x1000
#define PAGE_SHIFT	0xC
#define PAGE_ADDR(x)	((x) << PAGE_SHIFT)
#define PAGE_ALIGN_MASK 0xFFF
#define PAGE_ALIGN(x) ((((x - 1) >> 0xC) << 0xC) + 0x1000)

/* On a 32bit system, that's the maximum number of 8 bits pages we can have.
 * ((4GB / 4BK) / 8)
 */
#define MAX_BITMAP 0x20000

/* This could probably go in a more general header.
 * It clears/sets a bit depending on y which is an index.
 * It's not the real value of the bit.
 * So if you want to clear the third bit, y == 2 and not 4.
 */
#define SET_BIT(x, y)	((x |= (1 << y)))
#define CLEAR_BIT(x, y) ((x &= ~(1 << y)))

/* Takes a frame, divide it by 4096 and then by 8 */
#define GET_FRAME_BITMAP(x)	((x >> PAGE_SHIFT) >> 3)
#define GET_BITMAP_BIT_INDEX(x) ((x >> PAGE_SHIFT) % 8)
#define GET_BITMAP_ADDR(x, y) ((x << 0xF) + (y << 0xC))
#define FRAME_FREE(x)	get_free_frame(x)
#define FRAME_ANY	(uint32_t)-1

#define FRAME_ERR	(uint32_t)-1

#define PT_ADDRESS(x)	(x & 0xFFFFF000)
#define PTE_INDEX(x)	(x & 0x3FF)

#define PAGE_SET(x, y) (x |= y)

#define PAGE_PRESENT	0x1
#define PAGE_WRITE	0x2
#define PAGE_USER	0x4
#define PAGE_WRTHROUGH	0x8
#define PAGE_CACHEDIS	0x10
#define PAGE_DIRTY	0X20
#define PAGE_BIG	0x80
#define PAGE_GLOBAL	0x100

#define VMM_BASEVADDR	0xBFC00000
#define VMM_BASEPADDR	0x400000

#define _1MB_		0x0100000
#define _4MB_		0x0400000
#define _8MB_		0x0800000
#define _64MB_		0x3D00000

extern uint32_t _kernel_start[];
extern uint32_t _kernel_end[];
extern uint32_t _KERNEL_BASE[];

extern uint32_t _kernel_text_start[];
extern uint32_t _kernel_rodata_start[];
extern uint32_t _kernel_data_start[];
extern uint32_t _kernel_bss_start[];

extern uint32_t _kernel_text_end[];
extern uint32_t _kernel_rodata_end[];
extern uint32_t _kernel_data_end[];
extern uint32_t _kernel_bss_end[];

#define KERNEL_START (void *)_kernel_start
#define KERNEL_END (void *)_kernel_end
#define KERNEL_BASE (void *)_KERNEL_BASE
#define KSECT(x, y) (uint32_t)_kernel_##x##_##y

#define HEAP_BASE	KERNEL_END
#define HEAP_SIZE	_64MB_
#define HEAP_END	(uint32_t)(HEAP_BASE + HEAP_SIZE)

#define STACK_BASE	HEAP_END
#define STACK_SIZE	_8MB_
#define STACK_END	(uint32_t)(STACK_BASE + STACK_SIZE)
#define STACK_PTR	(STACK_END - 0x10)

#define KSECT_SIZE(x)	(KSECT(x, end) - KSECT(x, start))
#define KSECT_PHYS(x)	(KSECT(x, start) - (uint32_t)KERNEL_BASE)

void		frames_init(uint32_t low, uint32_t high);
void		paging_init();

uint32_t	get_free_frame(uint32_t n);
void		*allocate_frame(uint32_t frame);
void		free_frame(uint32_t frame);

int		map_page(uint32_t phys, uint32_t virt, uint32_t flags);
int		map_page_range(uint32_t phys, uint32_t virt, uint32_t cnt,
			       uint32_t flags);
int		unmap_page(uint32_t page);

void		stack_init(void);
void		heap_init(void);

void		memory_init(uint32_t low, uint32_t high);

#endif
