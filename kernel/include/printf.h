#ifndef PRINTK_H
#define PRINTK_H

#include <types.h>
#include <tty.h>
#include <string.h>
#include <stdarg.h>
#include <puts.h>
#include <macro.h>
#include <num.h>

#define ERED		VGA_COLOR_RED
#define EBLACK		VGA_COLOR_BLACK
#define EBLUE		VGA_COLOR_BLUE
#define EGREEN		VGA_COLOR_GREEN
#define ECYAN		VGA_COLOR_CYAN
#define EMAGENTA	VGA_COLOR_MAGENTA
#define EBROWN		VGA_COLOR_BROWN
#define ELIGHTGRAY	VGA_COLOR_LIGHTGRAY
#define EDARKGRAY	VGA_COLOR_DARKGRAY
#define ELIGHTBLUE	VGA_COLOR_LIGHTBLUE
#define ELIGHTGREEN	VGA_COLOR_LIGHTGREEN
#define ELIGHTCYAN	VGA_COLOR_LIGHTCYAN
#define ELIGHTRED	VGA_COLOR_LIGHTRED
#define EPINK		VGA_COLOR_PINK
#define EYELLOW		VGA_COLOR_YELLOW
#define EWHITE		VGA_COLOR_WHITE

#define EDEFAULTFG	VGA_COLOR_LIGHTGRAY
#define EDEFAULTBG	VGA_COLOR_BLACK


/* Very basic version of a printf like.
   No precision or minimum field width

   Available conversions :
   	%c (char)
	%d (int)
	%u (unsigned int)
	%s (char *s)
	%o (unsigned int in octal)
	%x (unsigned int in hex lowercase)
	%X (unsigned int in hex uppercase)
	%e (change foreground color)
	%E (change background color)
	%b (binary format)
	%p (pointer)
	%% (just prints %)
*/

struct	s_conv
{
	char specifier;
	void (*func)(const struct s_conv *, va_list *ap);
};

int	printf(const char *fmt, ...);

#endif
