#ifndef MACRO_H
#define MACRO_H

#define COUNT_OF(ptr) (sizeof(ptr) / sizeof(ptr[0]))

#define ABS(x) ((x < 0) ? -x : x)

#endif
