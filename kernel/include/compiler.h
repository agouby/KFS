#ifndef COMPILER_H
#define COMPILER_H

#define likely(x)	__builtin_expect(!!(x), 1)
#define unlikely(x)	__builtin_expect(!!(x), 0)

#define __unused	__attribute__((unused))
#define __align(x)	__attribute__((__aligned__(x)))
#define __noreturn	__attribute__((noreturn))
#define __always_inline __attribute__((always_inline))
#define __weak		__attribute__((weak))

#define asm		__asm__

#endif
