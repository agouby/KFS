#ifndef VGA_H
#define VGA_H

#include <types.h>

#define VGA_BASEADDR	0xC00B8000
#define VGA_HEIGHT	25
#define VGA_WIDTH	80
#define VGA_BUFSIZE	(VGA_HEIGHT * VGA_WIDTH)
#define CRTC_ADDR(x)	(x)
#define CRTC_DATA(x)	(x + 1)

#define VGA_GETPOS(x, y)	(VGA_WIDTH * y + x)

enum e_vga_color
{
	VGA_COLOR_BLACK,
	VGA_COLOR_BLUE,
	VGA_COLOR_GREEN,
	VGA_COLOR_CYAN,
	VGA_COLOR_RED,
	VGA_COLOR_MAGENTA,
	VGA_COLOR_BROWN,
	VGA_COLOR_LIGHTGRAY,
	VGA_COLOR_DARKGRAY,
	VGA_COLOR_LIGHTBLUE,
	VGA_COLOR_LIGHTGREEN,
	VGA_COLOR_LIGHTCYAN,
	VGA_COLOR_LIGHTRED,
	VGA_COLOR_PINK,
	VGA_COLOR_YELLOW,
	VGA_COLOR_WHITE,
	VGA_COLOR_MAX /* Always keep this one last */
};

static inline uint8_t vga_color(enum e_vga_color fg, enum e_vga_color bg)
{
        return (fg | bg << 4);
}

static inline uint8_t vga_getforegroundcolor(uint8_t color)
{
        return (color & 0xF);
}

static inline uint8_t vga_getbackgroundcolor(uint8_t color)
{
        return ((color >> 4) & 0x7);
}

static inline uint16_t vga_entry(unsigned char c, uint8_t color)
{
        return (uint16_t)c | (uint16_t)color << 8;
}

extern uint16_t	*vga_baseaddr;

void	vga_fillscreen(uint16_t entry);
void	vga_clearscreen(void);
void	vga_disablecursor(void);
void	vga_enablecursor(uint8_t start, uint8_t end);
uint16_t vga_crtc(void);

#endif
