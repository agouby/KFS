#ifndef PIC_H
#define PIC_H

#include <types.h>
#include <io.h>

#define PIC_MASTER_CMD	0x20
#define PIC_MASTER_DATA	0x21
#define PIC_SLAVE_CMD	0xA0
#define PIC_SLAVE_DATA	0xA1

#define PIC_EIO		0x20

#define ICW1_ENABLE		0b00000001
#define ICW1_ICW4_IS_PRESENT	0b00010000


/* In protected mode, the 32 first interrupts are reserved (0x0..0x1F), so we start mapping PIC at 0x20 */
#define ICW2_MASTER_OFFSET	0x20
#define ICW2_SLAVE_OFFSET	0x28

#define ICW3_MASTER_IRQ2	0b00000100
#define ICW3_SLAVE_IRQ2		0b00000010

#define ICW4_80x86_MODE		0b00000001

/* MASKS */
/*
#define PIC_SYSTIME	0b0000000000000001
#define PIC_KEYBOARD	0b0000000000000010
#define PIC_SERIALCOM2	0b0000000000001000
#define PIC_SERIALCOM1	0b0000000000010000
#define PIC_LPTERM2	0b0000000000100000
#define PIC_FLOPPY		0b0000000001000000
#define PIC_LPTERM1	0b0000000010000000
#define PIC_RTC		0b0000000100000000
#define PIC_MOUSE		0b0001000000000000
#define PIC_MATH		0b0010000000000000
#define PIC_ATA1		0b0100000000000000
#define PIC_ATA2		0b1000000000000000
*/

#define PIC_SYSTIME	0x0
#define PIC_KEYBOARD	0x1
#define PIC_SERIALCOM2	0x3
#define PIC_SERIALCOM1	0x4
#define PIC_LPTERM2	0x5
#define PIC_FLOPPY	0x6
#define PIC_LPTERM1	0x7
#define PIC_RTC		0x8
#define PIC_MOUSE	0xC
#define PIC_MATH	0xD
#define PIC_ATA1	0xE
#define PIC_ATA2	0xF

void	pic_init(void);
void	pic_enable_mask(uint8_t line);
void	pic_disable_mask(uint8_t line);

#endif
