#ifndef LIST_H
#define LIST_H

struct list_head
{
	struct list_head *prev;
	struct list_head *next;
};

static inline void INIT_LIST_HEAD(struct list_head *list)
{
	list->next = list;
	list->prev = list;
}

static inline void _list_add(struct list_head *new,
			     struct list_head *prev,
			     struct list_head *next)
{
	prev->next = new;
	next->prev = new;
	new->next = next;
	new->prev = prev;
}

static inline void list_add(struct list_head *new, struct list_head *head)
{
	_list_add(new, head, head->next);
}

static inline void list_add_tail(struct list_head *new, struct list_head *head)
{
	_list_add(new, head->prev, head);
}

#endif
