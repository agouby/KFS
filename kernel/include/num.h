#ifndef NUM_H
#define NUM_H

#include <macro.h>

char *itoabase_buf(int n, char *bufend, int base);
char *utoabase_buf(unsigned int n, char *bufend, int base);

#endif
