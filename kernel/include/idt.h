#ifndef IDT_H
#define IDT_H

#include <types.h>

/* We should use around 32 isr, and the syscall isr on 128 (int 0x80)*/

#define IDT_BASEADDR	0xC0000000

#define IDT_NENTRIES	(128 + 1)

#define INT_GATE	0xE
#define DPL_KERNEL	0
#define DPL_USER	3

struct s_idt_entry
{
	uint16_t offset_low;
	uint16_t selector;
	uint8_t zero;
	uint8_t type_attr;
	uint16_t offset_high;
} __attribute((packed));

struct s_idt_ptr
{
	uint16_t limit;
	uint32_t base;
} __attribute__((packed));

void	_idt_flush(void);
void	_idt_wrapper(void);

void	idt_set(void);

#endif
