#ifndef STRING_H
#define STRING_H

#include <tty.h>

void	*memset(void *s, unsigned char c, size_t n);
void	*memcpy(void *dest, const void *src, size_t n);
void	*memmove(void *dst, const void *src, size_t len);

size_t	strlen(const char *s);

char	*strchr(const char *s, char c);

#endif
