#ifndef GDT_H
#define GDT_H

#include <types.h>

#define GDT_BASEADDR	0xC0000800

struct s_gdt_entry
{
	uint16_t limit;
	uint16_t base_low;	// The lower 16 bits of the base.
	uint8_t  base_mid;	// The next 8 bits of the base.
	uint8_t  access;	// Access flags, determine what ring this segment can be used in.
	uint8_t  granularity;
	uint8_t  base_high;	// The last 8 bits of the base.
} __attribute__((packed));

struct s_gdt_ptr
{
	uint16_t limit;		// The upper 16 bits of all selector limits.
	uint32_t base;		// The address of the first gdt_entry_t struct.
} __attribute__((packed));

void	gdt_set_entry(struct s_gdt_entry *entry, uint32_t base, uint32_t limit, uint8_t access);
void	gdt_set(void);

void	_gdt_flush(const struct s_gdt_ptr *ptr);

#endif
