#ifndef TTY_H
#define TTY_H

#include <vga.h>
#include <string.h>

#define MAXTTY 10

struct	s_tty
{
	uint16_t buffer[VGA_BUFSIZE];
	uint8_t color;
	uint8_t x;
	uint8_t y;
};

extern struct s_tty	tty[MAXTTY];
extern struct s_tty	*tty_current;

static inline void	tty_setcolor(struct s_tty *tty, uint8_t color)
{
	tty->color = color;
}

static inline void	tty_setforegroundcolor(struct s_tty *tty, uint8_t newfg)
{
	uint8_t bg;

	bg = vga_getbackgroundcolor(tty->color);
	tty->color = vga_color(newfg, bg);
}

static inline void	tty_setbackgroundcolor(struct s_tty *tty, uint8_t newbg)
{
	uint8_t fg;

	fg = vga_getforegroundcolor(tty->color);
	tty->color = vga_color(fg, newbg);
}

void	tty_setup(void);
void	tty_fillscreen(struct s_tty *tty, int8_t c, uint16_t color);
void	tty_clearscreen(struct s_tty *tty);
size_t	tty_scrollup(struct s_tty *tty);

#endif
