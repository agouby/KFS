export PREFIX	= i386-elf

export CC	= $(PREFIX)-gcc
export AS	= nasm
export LD	= $(PREFIX)-ld
export OBJCOPY	= $(PREFIX)-objcopy

export CFLAGS	= -Wall -Wextra -std=c11 -ffreestanding -fno-builtin -fno-stack-protector -nostdlib -nodefaultlibs -g -fno-pic -O0
export ASFLAGS	= -f elf
